use crate::*;
use openapi_data::*;
use serde::{Deserialize, Serialize};
use specs::storage::VecStorage;
use specs::Component;
use specs::Entity;
use vek::*;

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "C", description = "伺服器回傳玩家創建是否成功")]
pub struct CS_Build {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示蓋塔有沒有成功")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "U", description = "客戶端請求升級塔")]
pub struct UC_Upgrade {
    #[mqtt_data(description = "請求升級的client id")]
    pub pname: String,
    #[mqtt_data(description = "要升級的塔uid")]
    pub tower_uid: u32
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "U", description = "伺服器回傳升級塔是否成功")]
pub struct US_Upgrade {
    #[mqtt_data(example = "ok", description = "回傳升級結果 ok/not_enough_coin 表示成功或金錢不足")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "D", description = "客戶端請求賣塔")]
pub struct DC_Sell {
    #[mqtt_data(description = "請求賣塔的client id")]
    pub pname: String,
    #[mqtt_data(description = "要賣的塔uid")]
    pub tower_uid: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "D", description = "伺服器回傳賣塔是否成功")]
pub struct DS_Sell {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示賣塔有沒有成功")]
    pub msg: String,
}


#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "R", description = "客戶端請求蓋塔價格")]
pub struct RC_Price {
    #[mqtt_data(description = "請求查詢的client id")]
    pub pname: String,
    #[mqtt_data(example = 0, description = "要查詢的塔靜態uid")]
    pub tower_uid: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "R", description = "伺服器回傳蓋塔價格")]
pub struct RS_Price {
    #[mqtt_data(example = "ok", description = "回傳ok/uid_miss_match 表示此次查詢是否成功不成功的話表示uid錯誤")]
    pub msg: String,
    #[mqtt_data(example = 100, description = "回傳塔的價格 如果查詢失敗回傳-1")]
    pub price: i32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "R", description = "客戶端請求升級塔價格")]
pub struct RC_UpgradeInfo {
    #[mqtt_data(description = "請求查詢的client id")]
    pub pname: String,
    #[mqtt_data(example = 0, description = "要查詢升級價格的塔uid")]
    pub tower_uid: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "R", description = "伺服器回傳升級塔價格")]
pub struct RS_UpgradeInfo {
    #[mqtt_data(example = "ok", description = "回傳ok/uid_miss_match 表示此次查詢是否成功不成功的話表示uid有誤")]
    pub msg: String,
    #[mqtt_data(example = 100, description = "回傳塔的升級價格 如果失敗的話回傳-1")]
    pub price: i32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "R", description = "客戶端請求自己蓋的塔UIDs")]
pub struct RC_OwnTower {
    #[mqtt_data(example = "XXX", description = "ClientID")]
    pub pname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "R", description = "伺服器回傳請求者蓋的塔UIDs")]
pub struct RS_OwnTower {
    #[mqtt_data(example = "[0, 1]", description = "回傳玩家蓋的塔UID陣列")]
    pub towers_uid: Vec::<u32>,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "player", act = "R", description = "客戶端請求遊戲內可用資源 ex: 金錢、可以蓋的塔 etc...")]
pub struct RC_ResourceInfo {
    #[mqtt_data(description = "查詢可用資源的Client ID")]
    pub pname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "player", act = "R", description = "伺服器回傳玩家可用資源")]
pub struct RS_ResourceInfo {
    #[mqtt_data(description = "擁有的金錢")]
    pub coin: u32,
    #[mqtt_data(description = "等級")]
    pub plevel: u32,
    #[mqtt_data(description = "可以蓋幾座塔")]
    pub allow_tower_num: u32,
    #[mqtt_data(description = "可以蓋的塔靜態UIDs")]
    pub tower_uid: Vec::<String>,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "grid", act = "R", description = "客戶端請求Grid內的所以物件 ex: 投射物、塔、敵方單位 etc...")]
pub struct RC_GridInfo {
    #[mqtt_data(description = "Client ID")]
    pub pname: String,
    #[mqtt_data(description = "要請求的Grid ID")]
    pub grid_id: Vec2::<i32>,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "grid", act = "R", description = "伺服器回傳Grid內的所以物件 #東西太多待討論")]
pub struct RS_GridInfo {
    // 東西太多待討論
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "status", act = "U", description = "客戶端收到PlayerUstatusDataS一定回傳此訊息表示連線仍在繼續 如果此回傳有5次未抵達伺服器將判定客戶端已離線")]
pub struct UC_Status {
    #[mqtt_data(description = "回傳ok就對了")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "status", act = "U", description = "伺服器通知客戶端回傳連線狀態訊息")]
pub struct US_Status {
    #[mqtt_data(description = "Client ID", example = "request_con_status")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "login", act = "C", description = "客戶端請求登入")]
pub struct CC_Login {
    #[mqtt_data(description = "要登入的Client ID")]
    pub pname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "login", act = "C", description = "伺服器回傳登入是否成功")]
pub struct CS_Login {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示登入是否成功")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "logout", act = "D", description = "客戶端請求登出")]
pub struct DC_Logout {
    #[mqtt_data(description = "要登出的Client ID")]
    pub pname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "logout", act = "D", description = "伺服器回傳登出是否成功")]
pub struct DS_Logout {
    #[mqtt_data(description = "回傳ok/fail 表示登出是否成功", example = "ok")]
    pub msg: String,
}