use openapi_data::*;
use vek::*;
use serde::{Deserialize, Serialize};
use specs::storage::VecStorage;
use specs::Entity;
use specs::{saveload, Component, FlaggedStorage, NullStorage};

use crate::msg::*;
use crate::*;

// Create Server Entity
#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "C", description = "Server生成怪物")]
pub struct CS_Entity {
    // 投影变量id
    #[mqtt_data(example = "1", description = "Creep id")]
    pub id: u32,
    #[mqtt_data(example = "1", description = "Creep 種族")]
    pub kind: String,
    #[mqtt_data(example = "1", description = "Creep 血量")]
    pub hp: f32,
    #[mqtt_data(example = "1", description = "Creep 移動速度")]
    pub msd: f32,
    #[mqtt_data(example = "", description = "Creep 出生位置")]
    pub pos: Vec2::<f32>,
}
// Update Server Move
#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "SM", description = "Server停止怪物移動")]
pub struct US_StopMove {
    // 投影变量id
    #[mqtt_data(example = "1", description = "Creep id")]
    pub id: u32,
    #[mqtt_data(example = "", description = "Creep 目前位置")]
    pub pos: Vec2::<f32>,
}


// Update Server Move
#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "UM", description = "Server校正怪物移動")]
pub struct US_Move {
    // 投影变量id
    #[mqtt_data(example = "1", description = "Creep id")]
    pub id: u32,
    #[mqtt_data(example = "", description = "Creep 目前位置")]
    pub cur: Vec2::<f32>,
    #[mqtt_data(example = "", description = "Creep 移動目標位置")]
    pub tar: Vec2::<f32>,
}

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "UHp", description = "Server更新怪物血量")]
pub struct US_Hp {
    // 投影变量id
    #[mqtt_data(example = "1", description = "Creep id")]
    pub id: u32,
    #[mqtt_data(example = "150", description = "")]
    pub hp: f32,
}

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "U", description = "Server更新怪物移動速度")]
pub struct US_MoveSpeed {
    // 投影变量id
    #[mqtt_data(example = "1", description = "Creep id")]
    pub id: u32,
    #[mqtt_data(example = "", description = "Creep 目前位置")]
    pub cur: Vec2::<f32>,
    #[mqtt_data(example = "150", description = "新的移動速度")]
    pub msd: f32,
    #[mqtt_data(example = "150", description = "移動速度持續時間")]
    pub lifetime: f32,
}
// Delete Server Entity
#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "creep", act = "D", description = "Server刪除怪物")]
pub struct DS_Entity {
    #[mqtt_data(example = "1", description = "entity id")]
    pub id: u32,
}

