use openapi_data::*;
use vek::*;
use serde::{Deserialize, Serialize};
use specs::storage::VecStorage;
use specs::Entity;
use specs::{saveload, Component, FlaggedStorage, NullStorage};

use crate::msg::*;
use crate::*;

// 基礎身體資訊
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct BaseInfo {
    pub hp: f32,         // 目前血量
    pub mhp: f32,        // 最大血量
    pub msd: f32,        // 移動速度
    pub def_physic: f32, // 物理防禦
    pub def_magic: f32,  // 魔法防禦
}

impl Component for BaseInfo {
    type Storage = VecStorage<Self>;
}

//怪物資訊
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CreepInfo {
    pub move_dis: f32,   // 移動距離
}

impl Component for CreepInfo {
    type Storage = VecStorage<Self>;
}


// 目標資訊
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TargetInfo {
    // 移動目標
    pub mov: Option<Vec2<f32>>,
    // 要攻擊的目標
    pub atk: Option<Entity>,
    // 能攻擊的到，可能是距離近並且沒有負面狀態
    pub can_atk: bool,
}

impl Component for TargetInfo {
    type Storage = VecStorage<Self>;
}


#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub struct AttackInfo {
    pub patk: Vf32,        // 物攻
    pub matk: Vf32,        // 魔攻
    pub ratk: Vf32,        // 真實傷害
    pub asd: Vf32,         // 攻速/每幾秒攻擊一次
    pub range: Vf32,       // 射程
    pub atk_radius: Vf32,      // 傷害範圍
    pub asd_count: f32,    // 攻速計數器
    pub bullet_speed: f32, // 子彈速度
}

impl AttackInfo {
    pub fn new(patk: f32, matk: f32, ratk: f32, asd: f32, range: f32, radius: f32, bullet_speed: f32) -> Self {
        Self {
            patk: patk.into(),
            matk: matk.into(),
            ratk: ratk.into(),
            asd: asd.into(),
            asd_count: asd,
            range: range.into(),
            atk_radius: radius.into(),
            bullet_speed: bullet_speed,
        }
    }
}

impl Component for AttackInfo {
    type Storage = VecStorage<Self>;
}


#[derive(Copy, Clone, Debug, Deserialize, Serialize, MqttData)]
pub enum FocusSetting {
    Close,   // 最近的
    First,   // 最前面的
    Last,    // 最後面的
    LeastHP, // 最少血的
    MostHP,  // 最多血的
}
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub struct TowerInfo {
    pub hp: Vf32,    // hp
    pub block: i32,  // 目前檔幾人
    pub mblock: i32, // 最大檔幾人
    pub size: f32,   // 阻檔半徑
    pub focus: FocusSetting,
}

impl TowerInfo {
    pub fn new(hp: f32, block: i32, size: f32) -> Self {
        Self {
            hp: hp.into(),
            block: 0,
            mblock: block,
            size: size,
            focus: FocusSetting::Last,
        }
    }
}

impl Component for TowerInfo {
    type Storage = VecStorage<Self>;
}
