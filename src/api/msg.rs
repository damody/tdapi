use crossbeam_channel::{bounded, select, tick, Receiver, Sender};
use failure::Error;
use log::*;
use math::round;
use rust_decimal::Decimal;
use serde_derive::{Deserialize, Serialize};
use serde_json::json;
use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::hash::Hash;
use std::io;
use std::rc::Rc;
use std::time::{Duration, Instant, SystemTime};
use asyncapi::message_binding::MQTTMessageBinding;
use asyncapi::operation_binding::MQTTOperationBinding;
use asyncapi::schema::*;
use asyncapi::*;
use indexmap::IndexMap;
use openapi_derive::MqttData;
use openapi_data::*;
use crate::*;
use vek::Vec2;

#[derive(Copy, Clone, Debug, Deserialize, Serialize, MqttData)]
pub struct Vf32 {
    pub bv: f32,
    pub v: f32,
}
impl From<Vf32> for f32 {
    fn from(v: Vf32) -> Self {
        v.v
    }
}
impl From<f32> for Vf32 {
    fn from(v: f32) -> Self {
        Vf32 { bv: v, v: v }
    }
}
impl Vf32 {
    pub fn new(v: f32) -> Vf32 {
        Vf32 { bv: v, v: v }
    }
    pub fn val(&mut self) -> f32 {
        self.v
    }
    //還原
    pub fn reset(&mut self) -> &mut Vf32 {
        self.v = self.bv;
        self
    }
    //暫時乘上
    pub fn mul(&mut self, v: f32) -> &mut Vf32 {
        self.v *= v;
        self
    }
    //暫時加上
    pub fn add(&mut self, v: f32) -> &mut Vf32 {
        self.v += v;
        self
    }
    // v += mv*v
    pub fn add_mul(&mut self, v: f32) -> &mut Vf32 {
        self.v += self.bv * v;
        self
    }
    pub fn clamp(&mut self, minv: f32, maxv: f32) -> &mut Vf32 {
        self.v = self.v.min(maxv).max(minv);
        self
    }
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub struct Val<T> {
    pub bv: T,
    pub mv: T,
    pub v: T,
}

impl<T> Val<T>
where
    T: Copy + Ord + std::ops::MulAssign + std::ops::AddAssign,
{
    fn new(v: T) -> Val<T> {
        Val { bv: v, mv: v, v: v }
    }

    //還原
    fn reset(&mut self) -> &mut Val<T> {
        self.v = self.bv;
        self
    }
    //暫時乘上
    fn mul(&mut self, v: T) -> &mut Val<T> {
        self.v *= v;
        self.v = self.v.max(self.mv);
        self
    }
    //暫時加上
    fn add(&mut self, v: T) -> &mut Val<T> {
        self.v += v;
        self.v = self.v.max(self.mv);
        self
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PlayerData {
    pub name: String,
    pub t: String,
    pub a: String,
    pub d: serde_json::Value,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CreepRawData {
    pub name: String,
    pub path: String,
    pub pidx: usize,
    pub money: f32, // 擊殺獲得金錢
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CreepData {
    pub pos: Vec2<f32>,
    pub name: String,
    pub path: String,
    pub pidx: usize,
    pub money: f32, // 擊殺獲得金錢
    pub cdata: BaseInfo,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TowerData {
    pub tname: String,
    pub tpty: TowerInfo,
    pub tatk: AttackInfo,
}


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct HeroData {
    pub pos: Vec2<f32>,
    pub name: String,
    pub binfo: BaseInfo,
    pub ainfo: AttackInfo,
    pub tinfo: TargetInfo,
}