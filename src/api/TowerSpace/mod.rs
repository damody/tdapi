use openapi_data::*;
use vek::*;
use serde::{Deserialize, Serialize};
use crate::api::Vf32;
use crate::GenericSpace::*;
use crate::msg::*;
use specs::storage::VecStorage;
use specs::{Component, Entity, FlaggedStorage, NullStorage};

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "C", description = "新的塔出現的資料")]
pub struct CS_Entity {
    #[mqtt_data(example = "john", description = "owner id")]
    pub clname: String,
    #[mqtt_data(example = "777", description = "tower ecs id")]
    pub tuid: u32,
    #[mqtt_data(example = "ice tower", description = "tower名稱")]
    pub tname: String,
    #[mqtt_data(example = "500.0", description = "生命值")]
    pub hp: Vf32,
    #[mqtt_data(example = "", description = "生命值")]
    pub pos: Vec2::<f32>,
    #[mqtt_data(example = "0", description = "阻檔数量")]
    pub block: i32,
    #[mqtt_data(example = "3", description = "最大阻檔数量")]
    pub mblock: i32,
    #[mqtt_data(example = "30.0", description = "塔半径")]
    pub size: f32,
    #[mqtt_data(example = "0", description = "塔焦點")]
    pub focus: FocusSetting,
    #[mqtt_data(example = "10.0", description = "物理攻击点数")]
    pub patk: Vf32,
    #[mqtt_data(example = "15.0", description = "魔法攻击点数")]
    pub matk: Vf32,
    #[mqtt_data(example = "0.0", description = "真实伤害点数")]
    pub ratk: Vf32,
    #[mqtt_data(example = "0.8", description = "攻击速度（每X秒攻击一次）")]
    pub asd: Vf32,
    #[mqtt_data(example = "700.0", description = "攻击范围")]
    pub range: Vf32,
}


#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "U", description = "客戶端請求修改塔的鎖定目標方式")]
pub struct UC_Focus {
    #[mqtt_data(description = "請求設定的client id")]
    pub clname: String,
    #[mqtt_data(description = "要設定的塔uid")]
    pub tuid: u32,
    #[mqtt_data(description = "新的FocusSetting設定用於指定的塔")]
    pub focus: i32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "U", description = "伺服器回傳修改塔的鎖定目標方式是否成功")]
pub struct US_Focus {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示設定FocusSetting有沒有成功")]
    pub msg: String,
}


#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "C", description = "客戶端傳送蓋塔請求")]
pub struct CC_Build {
    #[mqtt_data(description = "player 的唯一名稱")]
    pub clname: String,
    #[mqtt_data(description = "tower 的唯一名稱")]
    pub tname: String,
    #[mqtt_data(description = "tower 要放置的位置")]
    pub pos: Vec2::<f32>,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "C", description = "伺服器回傳蓋塔是否成功")]
pub struct CS_Build {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示蓋塔有沒有成功")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "U", description = "客戶端請求升級塔")]
pub struct UC_Upgrade {
    #[mqtt_data(description = "請求升級的client id")]
    pub clname: String,
    #[mqtt_data(description = "要升級的塔uid")]
    pub tuid: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "U", description = "伺服器回傳升級塔是否成功")]
pub struct US_Upgrade {
    #[mqtt_data(example = "ok", description = "回傳升級結果 ok/not_enough_coin 表示成功或金錢不足")]
    pub msg: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "D", description = "客戶端請求賣塔")]
pub struct DC_Entity {
    #[mqtt_data(description = "請求賣塔的client id")]
    pub clname: String,
    #[mqtt_data(description = "要賣的塔uid")]
    pub tuid: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "DSell", description = "塔賣掉了")]
pub struct DS_SellEntity {
    #[mqtt_data(example = "1", description = "entity id")]
    pub id: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "D", description = "塔死了")]
pub struct DS_Entity {
    #[mqtt_data(example = "1", description = "entity id")]
    pub id: u32,
}


#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "R", description = "客戶端請求蓋塔價格")]
pub struct RC_Price {
    #[mqtt_data(description = "請求查詢的client id")]
    pub clname: String,
    #[mqtt_data(example = 0, description = "要查詢的塔靜態name")]
    pub tname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "R", description = "伺服器回傳蓋塔價格")]
pub struct RS_Price {
    #[mqtt_data(example = "ok", description = "塔的name")]
    pub tname: String,
    #[mqtt_data(example = 100, description = "回傳塔的價格 如果查詢失敗回傳-1")]
    pub price: i32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "R", description = "客戶端請求升級塔價格")]
pub struct RC_Upgrade {
    #[mqtt_data(description = "請求查詢的client id")]
    pub clname: String,
    #[mqtt_data(example = 0, description = "要查詢升級價格的塔uid")]
    pub tuid: u32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "R", description = "伺服器回傳升級塔價格")]
pub struct RS_Upgrade {
    #[mqtt_data(example = 6543, description = "回傳對應的tower ecs id")]
    pub tuid: u32,
    #[mqtt_data(example = 100, description = "回傳塔的升級價格 如果失敗的話回傳-1")]
    pub price: i32,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/client", typ = "tower", act = "R", description = "客戶端請求自己蓋的塔UIDs")]
pub struct RC_OwnerUids {
    #[mqtt_data(example = "XXX", description = "ClientID")]
    pub clname: String,
}

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "tower", act = "R", description = "伺服器回傳請求者蓋的塔UIDs")]
pub struct RS_Uids {
    #[mqtt_data(example = "[0, 1]", description = "回傳玩家蓋的塔UID陣列")]
    pub towers_uid: Vec::<u32>,
}
