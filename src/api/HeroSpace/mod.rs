use openapi_data::*;
use vek::*;
use serde::{Deserialize, Serialize};
use crate::api::Vf32;
use crate::GenericSpace::*;
use crate::msg::*;
use specs::storage::VecStorage;
use specs::{Component, Entity, FlaggedStorage, NullStorage};

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "hero", act = "C", description = "新的hero出現的資料")]
pub struct CS_Entity {
    #[mqtt_data(example = "john", description = "owner id")]
    pub clname: String,
    #[mqtt_data(example = "777", description = "hero ecs id")]
    pub euid: u32,
    #[mqtt_data(example = "big gun", description = "hero名稱")]
    pub hname: String,
    #[mqtt_data(example = "500.0", description = "生命值")]
    pub hp: Vf32,
    #[mqtt_data(example = "10.0", description = "物理攻击")]
    pub patk: Vf32,
    #[mqtt_data(example = "15.0", description = "魔法攻击")]
    pub matk: Vf32,
    #[mqtt_data(example = "0.0", description = "真实伤害点数")]
    pub ratk: Vf32,
    #[mqtt_data(example = "0.8", description = "攻击速度（每X秒攻击一次）")]
    pub asd: Vf32,
    #[mqtt_data(example = "700.0", description = "攻击范围")]
    pub range: Vf32,
}


#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "hero", act = "C", description = "伺服器回傳創建英雄是否成功")]
pub struct CS_Build {
    #[mqtt_data(example = "ok", description = "回傳ok/fail 表示創建英雄有沒有成功")]
    pub msg: String,
}

