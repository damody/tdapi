
use openapi_data::*;
use vek::*;
use serde::{Deserialize, Serialize};
use crate::msg::*;
use crate::*;
#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "projectile", act = "C", description = "指定型投射物，中途不會爆")]
pub struct CS_Target {
    // 投射物变量id
    #[mqtt_data(description = "投射物实体id")]
    pub id: u32,
    #[mqtt_data(description = "剩余时间")]
    pub lifetime: f32,
    #[mqtt_data(description = "所有者实体id")]
    pub owner: u32,
    // 如果有target就是指定技 不然就是指向技
    #[mqtt_data(description = "目标实体id")]
    pub target: u32, // 0 就是没有目标
    #[mqtt_data(description = "投射物傷害半径")]
    pub radius: f32,
    #[mqtt_data(description = "投射物出生位置")]
    pub pos: Vec2::<f32>,
    #[mqtt_data(description = "投射物移动速度")]
    pub msd: f32, // 移动速度
}

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "projectile", act = "C", description = "目標位置型投射物，中途不會爆")]
pub struct CS_TargetArea {
    // 投射物变量id
    #[mqtt_data(description = "投射物实体id")]
    pub id: u32,
    #[mqtt_data(description = "所有者实体id")]
    pub owner: u32,
    // 如果有target就是指定技 不然就是指向技
    #[mqtt_data(description = "目标实体id")]
    pub target: u32, // 0 就是没有目标
    #[mqtt_data(description = "投射物傷害半径")]
    pub radius: f32,
    #[mqtt_data(description = "投射物移动速度")]
    pub msd: f32, // 移动速度
    #[mqtt_data(description = "投射物出生位置")]
    pub pos: Vec2::<f32>,
    #[mqtt_data(description = "投射物移動目標")]
    pub tpos: Vec2::<f32>,
}

#[derive(Clone, Debug, Deserialize, Serialize, MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "projectile", act = "D")]
pub struct DS_Entity {
    // 投射物变量id
    #[mqtt_data(description = "投射物实体id")]
    pub id: u32,
}