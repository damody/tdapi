use crate::*;
use openapi_data::*;
use serde::{Deserialize, Serialize};
use specs::storage::VecStorage;
use specs::Component;
use specs::Entity;
use vek::*;

#[derive(MqttData)]
#[mqtt_data(topic = "td/{clientid}/server", typ = "loot", act = "C", description = "伺服器回傳戰利品")]
pub struct CS_Created {
    #[mqtt_data(description = "得到道具的 client id")]
    pub pname: String,

    #[mqtt_data(description = "掉落道具的怪物名稱")]
    pub from_name: String,

    #[mqtt_data(description = "掉落道具的怪物 id")]
    pub from_id: i32,

    #[mqtt_data(description = "道具名稱")]
    pub loot_name: String,

    #[mqtt_data(description = "道具等級")]
    pub rank: String,

    #[mqtt_data(description = "道具種類")]
    pub kind: String,

    #[mqtt_data(description = "元素屬性")]
    pub element: String,

    #[mqtt_data(description = "物攻")]
    pub patk: i32,

    #[mqtt_data(description = "魔攻")]
    pub matk: i32,

    #[mqtt_data(description = "暴擊率")]
    pub crit: i32,

    #[mqtt_data(description = "攻速")]
    pub atk_spd: i32,

    #[mqtt_data(description = "護甲")]
    pub def: i32,

    #[mqtt_data(description = "生命加成")]
    pub hp: i32,

    #[mqtt_data(description = "閃避率")]
    pub avoid: i32,

}




